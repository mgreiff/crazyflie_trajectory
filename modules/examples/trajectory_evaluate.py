"""This example demonstrates the trajectory interaction by loading a trajectory
allowing the user to interact with it.
"""
import os,sys,inspect
curdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
modulesdir = os.path.dirname(curdir)
rootdir = os.path.dirname(modulesdir)
sys.path.insert(0,modulesdir)
import time

from trajectorylib import Trajectory

if __name__ == "__main__":
    directory = os.path.join(rootdir, 'trajectories')
    fnname = 'z_1'
    fformat = 'json'
    trajectory = Trajectory(directory, fnname, fformat)
    trajectory.visualize_fixed(['x','y','z'], ['1D', '2D', '3D'])
    startTime = 0.0   
    syncTime = time.time() 
    while 1:
        time.sleep(0.03)
        t = time.time() - syncTime + startTime
        x, y, z, yaw = trajectory.evaluate_trajectory(t)
        cmdx = x[0:3].tolist()
        cmdy = y[0:3].tolist()
        cmdz = z[0:3].tolist()
        print z

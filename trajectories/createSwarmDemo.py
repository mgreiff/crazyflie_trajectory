# -*- coding: utf-8 -*-
import json 
import os
import sys
import numpy as np
import datetime
from math import pi, asin, sin, cos, sqrt


################################################################################
# SWARM DEMO WITH SPIRAL
################################################################################
now = datetime.datetime.now()

# Starting point
xoffset = 2.3
yoffset = 2.0
zoffset = 0.0
dsx = 0.7
dsy = 0.7
#roslaunch bitcraze_lps_estimator joystick_control.launch joy_dev:=/dev/input/js0
# starting points
xstart = [xoffset, xoffset + dsx, xoffset, xoffset - dsx]
ystart = [yoffset + dsy, yoffset, yoffset - dsy, yoffset]
zstart = [zoffset, zoffset, zoffset, zoffset]

# Operating height
zOperation = 1.3

# Spirals and sinusoids
drInner = 0.5
drOuter = 1 * dsx
phase = [0.0,0.0,pi,pi]

# Initial position for sinusoid movement
xSinusoid = [xoffset, xoffset, xoffset, xoffset]
ySinusoid = [yoffset + drInner, yoffset + 2 * drInner, yoffset - drInner, yoffset - 2 * drInner]

# Take a detour to the hand, shutoff and return
handX = xoffset + 1.5
handY = yoffset + 1.5
handZ = 1.5
handtime = 3.0
pausetime = 6.0
        
wInner = 3.0

startTime = 4.0
stepTime = 2.0

landTime = 4.0

number = [[13,13,15,1],
          [17,17,17,1],
          [13,13,15,1],
          [17,17,18,1]]
     
NX, NY, NZ, NYAW = number

for nn in range(4):
    trajectory = {
        'packets':[],
        'events':[],
        'settings':{
            'circular':[0,0,0,0],
            'number':number[nn],
        'info':'Generated on %s'%(now.strftime("%A %d. %B %Y"))
        }
    }
    time = 0
    indices = [0,0,0,0]
    # Sets yaw to zero at all times
    trajectory['packets'].append({'data':[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                                  'time' :startTime, 'type': 0, 'index': indices[3],
                                  'number':NYAW, 'dimension':3})  
    indices[3] += 1

    # Initial convergence
    trajectory['packets'].append({'data':[xstart[nn], 0.0, 0.0, 0.0, 0.0, 0.0],
                                  'time' :startTime, 'type': 0, 'index': indices[0],
                                  'number':NX, 'dimension':0})
    trajectory['packets'].append({'data':[ystart[nn], 0.0, 0.0, 0.0, 0.0, 0.0],
                                  'time' :startTime, 'type': 0, 'index': indices[1],
                                  'number':NY, 'dimension':1})
    trajectory['packets'].append({'data':[zstart[nn], 0.0, 0.0, 0.0, 0.0, 0.0],
                                  'time' :startTime, 'type': 0, 'index': indices[2],
                                  'number':NZ, 'dimension':2})
    indices[0] += 1
    indices[1] += 1
    indices[2] += 1
    time += startTime
    
    # Takeoff
    trajectory['packets'].append({'data':[xstart[nn], 0.0, 0.0, 0.0, 0.0, 0.0],
                                  'time' :stepTime, 'type': 0, 'index': indices[0],
                                  'number':NX, 'dimension':0})
    trajectory['packets'].append({'data':[ystart[nn], 0.0, 0.0, 0.0, 0.0, 0.0],
                                  'time' :stepTime, 'type': 0, 'index': indices[1],
                                  'number':NY, 'dimension':1})
    trajectory['packets'].append({'data':[zOperation, 0.0, 0.0, 0.0, 0.0, 0.0],
                                  'time' :stepTime, 'type': 0, 'index': indices[2],
                                  'number':NZ, 'dimension':2})
    indices[0] += 1
    indices[1] += 1
    indices[2] += 1
    time += stepTime
    
    # step movement in the plane
    for ii in range(4):
        trajectory['packets'].append({'data':[xstart[(nn + ii) % 4], 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :stepTime, 'type': 0, 'index': indices[0],
                                      'number':NX, 'dimension':0})
        trajectory['packets'].append({'data':[ystart[(nn + ii) % 4], 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :stepTime, 'type': 0, 'index': indices[1],
                                      'number':NY, 'dimension':1})
        trajectory['packets'].append({'data':[zOperation, 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :stepTime, 'type': 0, 'index': indices[2],
                                      'number':NZ, 'dimension':2})
        indices[0] += 1
        indices[1] += 1
        indices[2] += 1
        time += stepTime

    # anti-clockwise step movement in the plane
    for ii in range(5):
        trajectory['packets'].append({'data':[xstart[(nn - ii) % 4], 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :stepTime, 'type': 0, 'index': indices[0],
                                      'number':NX, 'dimension':0})
        trajectory['packets'].append({'data':[ystart[(nn - ii) % 4], 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :stepTime, 'type': 0, 'index': indices[1],
                                      'number':NY, 'dimension':1})
        trajectory['packets'].append({'data':[zOperation, 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :stepTime, 'type': 0, 'index': indices[2],
                                      'number':NZ, 'dimension':2})
        print [xstart[(ii-1) % 4], ystart[(ii-1) % 4]]
        indices[0] += 1
        indices[1] += 1
        indices[2] += 1
        time += stepTime
    
    # Move into position for sinusoids
    if nn == 1 or nn == 3:
        trajectory['packets'].append({'data':[xSinusoid[nn], 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :stepTime, 'type': 0, 'index': indices[0],
                                      'number':NX, 'dimension':0})
        trajectory['packets'].append({'data':[ySinusoid[nn], 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :stepTime, 'type': 0, 'index': indices[1],
                                      'number':NY, 'dimension':1})
        trajectory['packets'].append({'data':[zOperation, 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :stepTime, 'type': 0, 'index': indices[2],
                                      'number':NZ, 'dimension':2})
        indices[0] += 1
        indices[1] += 1
        indices[2] += 1
        time += stepTime

    # Outer sinusoids
    outerTime = 8.0
    outerW = (2.0*pi)/outerTime
    if nn == 1 or nn == 3:
        trajectory['packets'].append({'data':[drOuter, xoffset, outerW, 0.0+ phase[nn], 0.0, 0.0],
                                      'time' :outerTime, 'type': 2, 'index': indices[0],
                                      'number':NX, 'dimension':0})
        trajectory['packets'].append({'data':[drOuter, yoffset, outerW, pi/2.0+ phase[nn], 0.0, 0.0],
                                      'time' :outerTime, 'type': 2, 'index': indices[1],
                                      'number':NY, 'dimension':1})
        trajectory['packets'].append({'data':[zOperation, 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :outerTime, 'type': 0, 'index': indices[2],
                                      'number':NZ, 'dimension':2})
        indices[0] += 1
        indices[1] += 1
        indices[2] += 1
        time += outerTime

    innerW = 2.5
    innerTime = 17
    dt = (innerTime - (stepTime + outerTime))/2.0
    dz = 0.4
    if nn == 0 or nn == 2:
        trajectory['packets'].append({'data':[drInner, xoffset, innerW, 0.0 + phase[nn], 0.0, 0.0],
                                      'time' :innerTime, 'type': 2, 'index': indices[0],
                                      'number':NX, 'dimension':0})
        trajectory['packets'].append({'data':[-drInner, yoffset, innerW, -pi/2.0 + phase[nn], 0.0, 0.0],
                                      'time' :innerTime, 'type': 2, 'index': indices[1],
                                      'number':NY, 'dimension':1})
        trajectory['packets'].append({'data':[zOperation, 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :stepTime + outerTime, 'type': 0, 'index': indices[2],
                                      'number':NZ, 'dimension':2})
        indices[0] += 1
        indices[1] += 1
        indices[2] += 1
        trajectory['packets'].append({'data':[zOperation, dz/dt, 0.0, 0.0, 0.0, 0.0],
                                      'time' :dt, 'type': 1, 'index': indices[2],
                                      'number':NZ, 'dimension':2})
        indices[2] += 1
        trajectory['packets'].append({'data':[zOperation + dz, -dz/dt, 0.0, 0.0, 0.0, 0.0],
                                      'time' :dt, 'type': 1, 'index': indices[2],
                                      'number':NZ, 'dimension':2})
        indices[2] += 1
        time += innerTime

        trajectory['packets'].append({'data':[xstart[nn], 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :landTime, 'type': 0, 'index': indices[0],
                                      'number':NX, 'dimension':0})
        trajectory['packets'].append({'data':[ystart[nn], 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :landTime, 'type': 0, 'index': indices[1],
                                      'number':NY, 'dimension':1})
        trajectory['packets'].append({'data':[0.3, 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :landTime, 'type': 0, 'index': indices[2],
                                      'number':NZ, 'dimension':2})
        indices[0] += 1
        indices[1] += 1
        indices[2] += 1
        time += stepTime
    
    if nn == 1:
        trajectory['packets'].append({'data':[handX, 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :handtime+pausetime, 'type': 0, 'index': indices[0],
                                      'number':NX, 'dimension':0})
        trajectory['packets'].append({'data':[handY, 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :handtime+pausetime, 'type': 0, 'index': indices[1],
                                      'number':NY, 'dimension':1})
        trajectory['packets'].append({'data':[handZ, 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :handtime+pausetime, 'type': 0, 'index': indices[2],
                                      'number':NZ, 'dimension':2})
        indices[0] += 1
        indices[1] += 1
        indices[2] += 1
        time += handtime

        # Stop event
        trajectory['events'].append({'data':[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                                     'time' :time, 'type': 0, 'index': 6,
                                     'number':NE, 'dimension':1})
        # Start event
        trajectory['events'].append({'data':[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                                     'time' :time + pausetime/2, 'type': 1, 'index': 7,
                                     'number':NE, 'dimension':1})
        time += pausetime

        trajectory['packets'].append({'data':[xoffset, 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :handtime, 'type': 0, 'index': indices[0],
                                      'number':NX, 'dimension':0})
        trajectory['packets'].append({'data':[yoffset + drInner, 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :handtime, 'type': 0, 'index': indices[1],
                                      'number':NY, 'dimension':1})
        trajectory['packets'].append({'data':[zOperation + dz, 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :handtime, 'type': 0, 'index': indices[2],
                                      'number':NZ, 'dimension':2})
        indices[0] += 1
        indices[1] += 1
        indices[2] += 1
        time += handtime

    if nn == 3:
        risetime = 5.0
        trajectory['packets'].append({'data':[xoffset, 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :stepTime+risetime, 'type': 0, 'index': indices[0],
                                      'number':NX, 'dimension':0})
        trajectory['packets'].append({'data':[xoffset, 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :stepTime+risetime, 'type': 0, 'index': indices[1],
                                      'number':NY, 'dimension':1})
        trajectory['packets'].append({'data':[zOperation, 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :stepTime, 'type': 0, 'index': indices[2],
                                      'number':NZ, 'dimension':2})
        indices[0] += 1
        indices[1] += 1
        indices[2] += 1
        time += stepTime
        trajectory['packets'].append({'data':[zOperation, dz/risetime, 0.0, 0.0, 0.0, 0.0],
                                      'time' :risetime, 'type': 1, 'index': indices[2],
                                      'number':NZ, 'dimension':2})
        indices[2] += 1
        time += risetime

        dt = 2*handtime + pausetime - 2*stepTime
        trajectory['packets'].append({'data':[xstart[(nn-1)%4], 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :dt, 'type': 2, 'index': indices[0],
                                      'number':NX, 'dimension':0})
        trajectory['packets'].append({'data':[ystart[(nn-1)%4], 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :dt, 'type': 0, 'index': indices[1],
                                      'number':NY, 'dimension':1})
        trajectory['packets'].append({'data':[zOperation + dz, 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :dt, 'type': 1, 'index': indices[2],
                                      'number':NZ, 'dimension':2})
        indices[0] += 1
        indices[1] += 1
        indices[2] += 1
        time += dt

    if nn == 1 or nn == 3:
        dt = 6.8
        trajectory['packets'].append({'data':[drInner, xoffset, innerW, 0.0 + phase[nn], 0.0, 0.0],
                                      'time' :dt, 'type': 2, 'index': indices[0],
                                      'number':NX, 'dimension':0})
        trajectory['packets'].append({'data':[-drInner, yoffset, innerW, -pi/2.0 + phase[nn], 0.0, 0.0],
                                      'time' :dt, 'type': 2, 'index': indices[1],
                                      'number':NY, 'dimension':1})
        trajectory['packets'].append({'data':[zOperation+dz, -dz/dt, 0.0, 0.0, 0.0, 0.0],
                                      'time' :dt, 'type': 1, 'index': indices[2],
                                      'number':NZ, 'dimension':2})
        indices[0] += 1
        indices[1] += 1
        indices[2] += 1
        time += stepTime

        trajectory['packets'].append({'data':[xstart[nn], 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :landTime, 'type': 0, 'index': indices[0],
                                      'number':NX, 'dimension':0})
        trajectory['packets'].append({'data':[ystart[nn], 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :landTime, 'type': 0, 'index': indices[1],
                                      'number':NY, 'dimension':1})
        trajectory['packets'].append({'data':[0.3, 0.0, 0.0, 0.0, 0.0, 0.0],
                                      'time' :landTime, 'type': 0, 'index': indices[2],
                                      'number':NZ, 'dimension':2})
        indices[0] += 1
        indices[1] += 1
        indices[2] += 1
        time += stepTime

    print indices
    
    #### Events ###
    NE = 6
    # Reset Kalman
    trajectory['events'].append({'data':[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                                 'time' :0.0, 'type': 3, 'index': 0,
                                 'number':NE, 'dimension':0})
    # Controller settings
    trajectory['events'].append({'data':[0.0, 7.0, 0.0, 0.0, 0.0, 0.0],
                                 'time' :0.0, 'type': 2, 'index':1,
                                 'number':NE, 'dimension':0})
    trajectory['events'].append({'data':[1.0, 7.0, 0.0, 0.0, 0.0, 0.0],
                                 'time' :0.0, 'type': 2, 'index': 2,
                                 'number':NE, 'dimension':0})
    # Controller settings
    trajectory['events'].append({'data':[2.0, 7.0, 0.0, 0.0, 0.0, 0.0],
                                 'time' :0.0, 'type': 2, 'index': 3,
                                 'number':NE, 'dimension':0})
    # Start
    trajectory['events'].append({'data':[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                                 'time' :startTime, 'type': 1, 'index': 4,
                                 'number':NE, 'dimension':1})
    # Stop
    trajectory['events'].append({'data':[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                                 'time' :time, 'type': 0, 'index': 5,
                                 'number':NE, 'dimension':1})

    # Saves the dictionary
    filename = 's_%s.json' % str(nn)
    with open(filename, 'w') as trajectoryFile:
        json.dump(trajectory, trajectoryFile, separators=(',', ':'),sort_keys=True, indent=4)
   


